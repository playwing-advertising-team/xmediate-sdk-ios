//
//  XMVideoAdViewController.m
//
//
//  Created by XMediate on 07/09/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import "XMVideoAdViewController.h"
#import "XMSDK.h"

@interface XMVideoAdViewController () <XMVideoAdDelegate>
@property (strong,nonatomic) XMVideoAd *xmVideoAd;
@property (weak, nonatomic) IBOutlet UIButton *loadAdButton;
@property (weak, nonatomic) IBOutlet UIButton *showAdButton;
@end

@implementation XMVideoAdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.loadAdButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.loadAdButton.titleLabel.numberOfLines = 2;
    self.loadAdButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self enableButton:self.loadAdButton];
    
    self.showAdButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.showAdButton.titleLabel.numberOfLines = 2;//if you want unlimited number of lines put 0
    self.showAdButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self disableButton:self.showAdButton];
    
    /// Create the Video ad object.
    self.xmVideoAd = [[XMVideoAd alloc] initWithDelegate:self];
}

-(void)dealloc {
    NSLog(@"XMVideoAdViewController dealloc");
}

- (IBAction)loadVideoAd:(id)sender {
    NSLog(@"XMediate : loadVideoAd()");
    [self.xmVideoAd loadWithSettings:[XMAdSettings settings]];
}

- (IBAction)showVideoAd:(id)sender {
    NSLog(@"XMediate : showVideoAd()");
    if([self.xmVideoAd isReady]){
        [self.xmVideoAd presentFromViewController:self];
    }else{
        NSLog(@"XMediate : Video ad not ready yet");
    }
}

- (void)videoAdDidLoad:(XMVideoAd *)videoAd{
    NSLog(@"XMediate : video ad loaded");
    [self enableButton:self.showAdButton];
    [self disableButton:self.loadAdButton];
}

- (void)videoAd:(XMVideoAd *)videoAd didFailToLoadWithError:(NSError *)error{
    NSLog(@"XMediate : video ad Failed to load");
}

- (void)videoAdWillPresent:(XMVideoAd *)videoAd{
    NSLog(@"XMediate : video ad will present");
}

- (void)videoAdDidPresent:(XMVideoAd *)videoAd{
    NSLog(@"XMediate : video ad did present");
    
}

- (void)videoAdDidStartPlaying:(XMVideoAd *)videoAd{
    NSLog(@"XMediate : video ad did start playing");
    
}

- (void)videoAd:(XMVideoAd *)videoAd didFailToPlayWithError:(NSError *)error{
    NSLog(@"XMediate : video failed to load with error %@",error);
}

- (void)videoAdWillDismiss:(XMVideoAd *)videoAd{
    NSLog(@"XMediate : video ad will dismiss");
}

- (void)videoAdDidDismiss:(XMVideoAd *)videoAd{
    NSLog(@"XMediate : video ad did dismiss");
    [self enableButton:self.loadAdButton];
    [self disableButton:self.showAdButton];
}

- (void)videoAdDidReceiveTapEvent:(XMVideoAd *)videoAd{
    NSLog(@"XMediate : video ad tap event");
}

- (void)videoAdWillLeaveApplication:(XMVideoAd *)videoAd{
    NSLog(@"XMediate : video ad leave application");
}

- (void)videoAdDidExpire:(XMVideoAd *)videoAd{
    NSLog(@"XMediate : video ad did expired");
    [self enableButton:self.loadAdButton];
    [self disableButton:self.showAdButton];
}

-(void)videoAdDidFinishPlayingVideoAd:(XMVideoAd *)videoAd {
    NSLog(@"XMediate : video ad did finish playing videoAd");
}

@end
