//
//  XMAdViewController.m
//  XMediateDempApp
//
//  Created by XMediate on 23/05/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMAdViewController.h"

@implementation XMAdViewController

-(void)viewDidLoad {
    [super viewDidLoad];
}


-(void)enableButton:(UIButton*)button {
    button.enabled = YES;
    button.backgroundColor = [UIColor colorWithRed:0.13 green:0.59 blue:0.95 alpha:1.0];
}

-(void)disableButton:(UIButton*)button {
    button.enabled = NO;
    button.backgroundColor = [UIColor colorWithRed:0.13 green:0.17 blue:0.22 alpha:0.8];
}


@end
