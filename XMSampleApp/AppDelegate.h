//
//  AppDelegate.h
//  XMSampleApp
//
//  Created by XMediate on 08/09/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

