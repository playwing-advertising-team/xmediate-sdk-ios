//
//  XMInterstitialAdViewController.m
//  XMediateDempApp
//
//  Created by XMediate on 07/09/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import "XMInterstitialAdViewController.h"
#import "XMSDK.h"

@interface XMInterstitialAdViewController ()<XMInterstitialAdDelegate>

//XMInterstitialAd Object
@property (strong, nonatomic)  XMInterstitialAd *xmInterstitialAd;

@property (weak, nonatomic) IBOutlet UIButton *loadButton;
@property (weak, nonatomic) IBOutlet UIButton *showButton;
@end

@implementation XMInterstitialAdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.loadButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.loadButton.titleLabel.numberOfLines = 2;
    self.loadButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self enableButton:self.loadButton];
    
    self.showButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.showButton.titleLabel.numberOfLines = 2;
    self.showButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self disableButton:self.showButton];
    
    /// Create the interstitial ad object.
    self.xmInterstitialAd = [[XMInterstitialAd alloc] initWithDelegate:self];
}

- (IBAction)loadButtonClicked:(UIButton *)sender {
    // To load XMediate Interstitial Ad
    NSLog(@"XMediate : load interstitial ad");
    [self.xmInterstitialAd loadWithSettings:[XMAdSettings settings]];
}


- (IBAction)showButtonClicked:(UIButton *)sender {
    NSLog(@"XMediate : show interstitial ad");
    if([self.xmInterstitialAd isReady]){
        [self.xmInterstitialAd presentFromViewController:self];
    } else {
        NSLog(@"XMediate : interstitial ad not ready");
    }
}


- (void)interstitialDidLoadAd:(XMInterstitialAd *)interstitial {
    NSLog(@"XMediate : Interstitial Ad Loaded.");
    [self enableButton:self.showButton];
    [self disableButton:self.loadButton];
}

- (void)interstitial:(XMInterstitialAd *)interstitial didFailToLoadAdWithError:(NSError *)error {
    NSLog(@"XMediate : Interstitial Ad Loading failed.");
}

- (void)interstitialWillPresent:(XMInterstitialAd *)interstitial {
    NSLog(@"XMediate : Interstitial Will Present.");
}

- (void)interstitialDidPresent:(XMInterstitialAd *)interstitial {
    NSLog(@"XMediate : Interstitial did present.");
}

- (void)interstitialWillDismiss:(XMInterstitialAd *)interstitial {
    NSLog(@"XMediate : Interstitial will dismiss.");
}

- (void)interstitialDidDismiss:(XMInterstitialAd *)interstitial {
    NSLog(@"XMediate : Interstitial did dismiss.");
    [self enableButton:self.loadButton];
    [self disableButton:self.showButton];
}

- (void)interstitialDidExpire:(XMInterstitialAd *)interstitial {
    NSLog(@"XMediate : Interstitial did expire.");
    [self enableButton:self.loadButton];
    [self disableButton:self.showButton];
}

- (void)interstitialDidReceiveTapEvent:(XMInterstitialAd *)interstitial {
    NSLog(@"XMediate : Interstitial did receive tap event.");
}

- (void)interstitialWillLeaveApplication:(XMInterstitialAd *)interstitial {
    NSLog(@"XMediate : Interstitial will leave application.");
}

@end
