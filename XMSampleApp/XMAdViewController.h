//
//  XMAdViewController.h
//  XMediateDempApp
//
//  Created by XMediate on 23/05/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMAdViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *logTextView;
-(void)enableButton:(UIButton*)button;
-(void)disableButton:(UIButton*)button;
@end
