//
//  XMAdListViewController.m
//  XMediateDempApp
//
//  Created by XMediate on 05/04/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import "XMAdListViewController.h"
#import "XMBannerAdViewController.h"

@interface XMAdListViewController ()
@property (nonatomic, strong) NSArray *adTypeSections;
@end


@implementation XMAdListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"XMediate Ads";
    
}

- (NSArray *)adTypeSections {
    return @[@"Banner Ads",@"Interstitial Ads",@"RewardedVideo Ads",@"Video Ads"];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.adTypeSections count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"adtypecell" forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.text = self.adTypeSections[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIViewController *detailViewController = nil;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    switch (indexPath.row) {
        case 0:
            detailViewController = [storyboard instantiateViewControllerWithIdentifier:@"banneradviewcontroller"];
            break;
            
        case 1:
            detailViewController = [storyboard instantiateViewControllerWithIdentifier:@"interstitialadviewcontroller"];
            break;
            
        case 2:
            detailViewController = [storyboard instantiateViewControllerWithIdentifier:@"rewardedvideoadviewcontroller"];
            break;
            
        case 3:
            detailViewController = [storyboard instantiateViewControllerWithIdentifier:@"videoadviewcontroller"];
            break;
        default:
            break;
    }
    if (detailViewController) {
        [self.navigationController showViewController:detailViewController sender:self];
        //    [self.navigationController pushViewController:detailViewController animated:YES];
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
