//
//  ViewController.m
//  XMediateDempApp
//
//  Created by XMediate on 07/09/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import "XMBannerAdViewController.h"
#import "XMSDK.h"
#import <WebKit/WebKit.h>
#import <objc/runtime.h>

@interface XMBannerAdViewController () <XMBannerAdViewDelegate>

@property (strong, nonatomic)  UIView *xMediateSmallBannerContainer;
//XMediate Banner Views
@property (strong, nonatomic)  XMBannerAdView *xmBannerView;
@property (strong, nonatomic)  XMBannerAdView *xMediateLargeBanner;
@property (strong, nonatomic)  XMBannerAdView *xMediateMRECTBanner;
@property (strong, nonatomic)  XMBannerAdView *currentXMediateBanner;

@property (weak, nonatomic) IBOutlet UILabel *adPositionLabel;
@property (weak, nonatomic) IBOutlet UIButton *loadBannerButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segamentedControl;
@property (assign, nonatomic) NSUInteger requestCount;
@property (weak, nonatomic) IBOutlet UIImageView *adImageView;
@property (nonatomic,strong) XMAdSettings* adSettings;

@end

@implementation XMBannerAdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.scrollView.backgroundColor = [UIColor yellowColor];
    self.loadBannerButton.layer.cornerRadius = 5.0;
    
    self.xmBannerView = [[XMBannerAdView alloc] initWithFrame:CGRectZero];
    [self setUpBannerView:self.xmBannerView];
    
    self.xMediateLargeBanner = [[XMBannerAdView alloc] initWithFrame:CGRectZero];
    [self setUpBannerView:self.xMediateLargeBanner];
    
    self.xMediateMRECTBanner = [[XMBannerAdView alloc] initWithFrame:CGRectZero];
    [self setUpBannerView:self.xMediateMRECTBanner];
    
    /**
     *Ad Setting objects
     */
    XMAdSettings* settings = [XMAdSettings settings];
    settings.gender = XMGenderMale;
    settings.maritalStatus = XMMaritalStatusMarried;
    settings.sexualOrientation = XMSexualOrientaionStraight;
    settings.age = 33;
    settings.income = 1000000;
    settings.education = @"Graduate";
    settings.language = @"English";
    settings.keywords = @"electronics,sports,movies,music";
    settings.extraParams = @{@"car":@"i20"};
    [settings setDateOfBirthWithMonth:10 day:9 year:1983];
    settings.testDevices = @[@"26f1b37ca712f9ae72f64d435e92e7cae19128a1"];
    
    self.adSettings = settings;
}

/**
 * Set viewController and Delegate
 */
-(void) setUpBannerView:(XMBannerAdView*)xmBannerView {
    xmBannerView.backgroundColor = [UIColor redColor];
    xmBannerView.viewControllerForPresentingModal = self;
    xmBannerView.delegate = self;
    [self.scrollView addSubview:xmBannerView];
}


- (IBAction)segamentChanged:(UISegmentedControl *)sender {
    NSInteger index = sender.selectedSegmentIndex;
    [self moveToPage:index];
}

-(void) moveToPage:(NSInteger)page {
    
    CGFloat pageWidth = self.view.frame.size.width;
    CGFloat slideToX = page*pageWidth;
    [self.scrollView scrollRectToVisible:CGRectMake(slideToX, 0,pageWidth, self.scrollView.frame.size.height) animated: true];
}

- (IBAction)loadBannerClicked:(UIButton *)sender {
    NSInteger index = self.segamentedControl.selectedSegmentIndex;
    NSLog(@"---------------------------------------------------------\n");
    NSLog(@"XMediate : load banner called");
    switch (index) {
        case 0:
            self.currentXMediateBanner = self.xmBannerView;
            [self.xmBannerView loadWithSettings:self.adSettings];
            break;
        case 1:
            self.currentXMediateBanner = self.xMediateLargeBanner;
            [self.xMediateLargeBanner loadWithSettings:self.adSettings];
            break;
        case 2:
            self.currentXMediateBanner = self.xMediateMRECTBanner;
            [self.xMediateMRECTBanner loadWithSettings:self.adSettings];
            break;
            
        default:
            break;
    }
}

-(void)viewWillLayoutSubviews {
    
    CGFloat scrollViewWidth = self.view.frame.size.width;
    CGFloat scrollViewHeight = self.scrollView.frame.size.height;
    
    self.scrollView.contentSize = CGSizeMake(scrollViewWidth * 3,self.scrollView.frame.size.height);
    
    self.xmBannerView.frame = CGRectMake(0, (scrollViewHeight-50.0)/2, scrollViewWidth, 50.0);
    self.xMediateLargeBanner.frame = CGRectMake(scrollViewWidth, (scrollViewHeight-100.0)/2, scrollViewWidth, 100.0);
    self.xMediateMRECTBanner.frame = CGRectMake(scrollViewWidth*2, 0, scrollViewWidth, 250.0);
    
    NSInteger index = self.segamentedControl.selectedSegmentIndex;
    self.scrollView.contentOffset = CGPointMake(index*scrollViewWidth, 0);
    [super viewWillLayoutSubviews];
}

- (void)bannerAdViewDidLoadAd:(XMBannerAdView *)view {
    NSLog(@"XMediate : Banner Ad View Loaded.");
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(void) {
                         [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:view cache:YES];
                     }
                     completion:^(BOOL finished) {
                     }];
}

- (void) loadAdAgain {
    if (self.requestCount >=30) {
        return;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.requestCount += 1;
        [self loadBannerClicked:nil];
    });
}

- (void)bannerAdViewDidFailToLoadAd:(XMBannerAdView *)view withError:(NSError*)error {
    NSLog(@"XMediate : Banner Ad View Loading failed with error: %@.",error);
}

- (void)willPresentFullScreenForAd:(XMBannerAdView *)view {
    NSLog(@"XMediate : Banner Ad View will Present FullScreen.");
}

- (void)didDismissFullScreenForAd:(XMBannerAdView *)view {
    NSLog(@"XMediate : Banner Ad View did dismiss FullScreen.");
}

- (void)willLeaveApplicationFromAd:(XMBannerAdView *)view {
    NSLog(@"XMediate : Banner Ad View will leave application.");
}

@end

