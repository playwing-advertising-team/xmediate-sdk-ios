//
//  XMRewardedVideoViewController.m
//  XMediateDempApp
//
//  Created by XMediate on 07/09/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import "XMRewardedVideoViewController.h"
#import "XMSDK.h"

@interface XMRewardedVideoViewController ()<XMRewardedVideoAdDelegate>
@property (strong, nonatomic)  XMRewardedVideoAd *xMediateRewardedAd;
@property (weak, nonatomic) IBOutlet UIButton *loadAdButton;
@property (weak, nonatomic) IBOutlet UIButton *showAdButton;
@end

@implementation XMRewardedVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.loadAdButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.loadAdButton.titleLabel.numberOfLines = 2;
    self.loadAdButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self enableButton:self.loadAdButton];
    
    self.showAdButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.showAdButton.titleLabel.numberOfLines = 2;//if you want unlimited number of lines put 0
    self.showAdButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self disableButton:self.showAdButton];
    
    /// Create the Rewarded Video ad object.
    self.xMediateRewardedAd = [[XMRewardedVideoAd alloc] initWithDelegate:self];
}

- (IBAction)loadRewardedVideoAd:(id)sender {
    NSLog(@"XMediate : load Rewarded Video ad");
    [self.xMediateRewardedAd loadWithSettings:[XMAdSettings settings]];
}

- (IBAction)showRewardedVideoAd:(id)sender {
    NSLog(@"XMediate : show Rewarded Video ad");
    if([self.xMediateRewardedAd isReady]){
        [self.xMediateRewardedAd presentFromViewController:self];
    }else{
        NSLog(@"XMediate : xm rewarded video ad not ready");
    }
}

- (void)rewardVideoAdDidLoad:(XMRewardedVideoAd *)rewardedVideoAd {
    NSLog(@"XMediate : RewardedVideo Ad Loaded.");
    [self enableButton:self.showAdButton];
    [self disableButton:self.loadAdButton];
}

- (void)rewardedVideoAd:(XMRewardedVideoAd *)rewardedVideoAd didFailToLoadWithError:(NSError *)error {
    NSLog(@"XMediate : RewardedVideo Ad failed with error %@.",error);
}

- (void)rewardedVideoAdWillPresent:(XMRewardedVideoAd *)rewardedVideoAd {
    NSLog(@"XMediate : rewardedVideoAdWillPresent.");
}

- (void)rewardedVideoAdDidPresent:(XMRewardedVideoAd *)rewardedVideoAd {
    NSLog(@"XMediate : rewardedVideoAdDidPresent.");
}

- (void)rewardedVideoAdDidStartPlaying:(XMRewardedVideoAd *)rewardedVideoAd {
    NSLog(@"XMediate : rewardedVideoAdDidStartPlaying.");
}

- (void)rewardedVideoAd:(XMRewardedVideoAd *)rewardedVideoAd didFailToPlayWithError:(NSError *)error {
    NSLog(@"XMediate : rewardedVideoAddidFailToPlay WithError %@",error);
}

- (void)rewardedVideoAdWillDismiss:(XMRewardedVideoAd *)rewardedVideoAd {
    NSLog(@"XMediate : rewardedVideoAdWillDismiss");
}

- (void)rewardedVideoAdDidDismiss:(XMRewardedVideoAd *)rewardedVideoAd {
    NSLog(@"XMediate : rewardedVideoAdDidDismiss");
    [self enableButton:self.loadAdButton];
    [self disableButton:self.showAdButton];
}

- (void)rewardedVideoAdDidReceiveTapEvent:(XMRewardedVideoAd *)rewardedVideoAd {
    NSLog(@"XMediate : rewardedVideoAdDidReceiveTapEvent");
}

- (void)rewardedVideoAdWillLeaveApplication:(XMRewardedVideoAd *)rewardedVideoAd {
    NSLog(@"XMediate : rewardedVideoAdWillLeaveApplication");
}

- (void)rewardedVideoAd:(XMRewardedVideoAd *)rewardedVideoAd didRewardUserWithReward:(XMRewardedVideoReward *)reward {
    NSLog(@"XMediate : rewardedVideoAd didRewardUserWithReward with reward %@",reward);
}

- (void)rewardedVideoAdDidExpire:(XMRewardedVideoAd *)rewardedVideoAd {
    NSLog(@"XMediate : rewardedVideoAdDidExpire");
}

@end
