//
//  XMInterstitialAd.h
//  XMediateSDK
//
//  Created by XMediate on 14/03/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMAdSettings.h"
#import "XMInterstitialAdDelegate.h"

@interface XMInterstitialAd : NSObject

-(instancetype) initWithDelegate:(id<XMInterstitialAdDelegate>)delegate;

/**
 * The delegate (`XMInterstitialAdDelegate`) of the interstitial ad object.
 */
@property (nonatomic, weak) id<XMInterstitialAdDelegate> delegate;

/**
 * A Boolean value that represents whether the interstitial ad has loaded an advertisement and is ready to be presented.
 *
 * After creating an interstitial ad object, you can use `loadAd` to tell the object to begin loading ad content. Once the content has been loaded, the value of this property will be YES.
 *
 * The value of this property can be NO if the ad content has not finished loading or has already been presented or has expired. The expiration condition only applies for ads from certain third-party ad networks. 
 *
 * See `XMInterstitialAdDelegate` for more details.
 */
@property(nonatomic, readonly, getter=isReady) BOOL ready;


/**
 * Requests a new ad.
 *
 * If the interstitial is already loading an ad or an interstitial is already presnted, this call will be ignored.
 */
-(void) loadWithSettings:(XMAdSettings*)adSettings;

/**
 * Presents the interstitial ad modally from the specified view controller.
 *
 * This method will do nothing if the interstitial ad has not been ready (i.e. the value of its `ready` property is NO).
 *
 * `XMInterstitialAdDelegate` provides optional methods that you may implement to stay
 * informed about when an interstitial takes over or leaves the screen.
 *
 * @param controller The view controller that should be used to present the interstitial ad.
 * This view controller must be added to the application key window before you call this method.
 */
- (void)presentFromViewController:(UIViewController *)controller;

@end
