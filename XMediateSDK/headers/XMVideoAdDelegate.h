//
//  XMVideoAdDelegate.h
//  XMediateSDK
//
//  Created by XMediate on 05/04/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>
@class XMVideoAd;

@protocol XMVideoAdDelegate <NSObject>

/**
 * Tells the delegate that a video ad was received.
 *
 * @param videoAd The video ad object.
 */
- (void)videoAdDidLoad:(XMVideoAd *)videoAd;

/**
 * Tells the delegate that the video ad failed to load.
 *
 * @param videoAd The video ad object.
 * @param error An error indicating why the ad failed to load.
 */
- (void)videoAd:(XMVideoAd *)videoAd didFailToLoadWithError:(NSError *)error;

/**
 * This method is called when a video ad is about to appear.
 *
 * @param videoAd The  video ad object.
 */
- (void)videoAdWillPresent:(XMVideoAd *)videoAd;

/**
 * This method is called when a  video ad has appeared.
 *
 * @param videoAd The  video ad object.
 */
- (void)videoAdDidPresent:(XMVideoAd *)videoAd;


/**
 * Tells the delegate that the  video ad started playing.
 *
 * @param videoAd The  video ad object.
 */
- (void)videoAdDidStartPlaying:(XMVideoAd *)videoAd;


/**
 * This method is called when a  video fails to play .
 *
 * @param videoAd The  video ad object.
 * @param error An error describing why the video couldn't play.
 */
- (void)videoAd:(XMVideoAd *)videoAd didFailToPlayWithError:(NSError *)error;


/**
 * This method is called when a  video ad will be dismissed.
 *
 * @param videoAd The  video ad object.
 */
- (void)videoAdWillDismiss:(XMVideoAd *)videoAd;

/**
 * This method is called when a  video ad has been dismissed.
 *
 * @param videoAd The  video ad object.
 */
- (void)videoAdDidDismiss:(XMVideoAd *)videoAd;

/**
 * This method is called when the user taps on the ad.
 *
 * @param videoAd The  video ad object.
 */
- (void)videoAdDidReceiveTapEvent:(XMVideoAd *)videoAd;

/**
 * This method is called when a  video ad will cause the user to leave the application.
 *
 * @param videoAd The  video ad object.
 */
- (void)videoAdWillLeaveApplication:(XMVideoAd *)videoAd;

/**
 * This method is called when a previously loaded  video is no longer eligible for presentation.
 *
 * @param videoAd The  video ad object.
 */
- (void)videoAdDidExpire:(XMVideoAd *)videoAd;

- (void)videoAdDidFinishPlayingVideoAd:(XMVideoAd *)videoAd;

@end
