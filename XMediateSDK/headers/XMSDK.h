//
//  XMediateSDK.h
//  XMediateSDK
//
//  Created by XMediate on 31/01/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMBannerAdView.h"
#import "XMBannerAdViewDelegate.h"
#import "XMAdSize.h"
#import "XMAdSettings.h"
#import "XMInterstitialAd.h"
#import "XMInterstitialAdDelegate.h"
#import "XMRewardedVideoAd.h"
#import "XMRewardedVideoAdDelegate.h"
#import "XMRewardedVideoReward.h"
#import "XMVideoAdDelegate.h"
#import "XMVideoAd.h"
#import "XMNativeAd.h"
#import "XMNativeAdLoader.h"
#import "XMNativeAdEventsDelegate.h"
#import "XMNativeAdLoaderDelegate.h"
#import "XMNativeAdImage.h"
#import "XMNativeAdViewProtocol.h"

@interface XMSDK : NSObject

+ (nonnull NSString*) version;
+ (void) initializeWithPublisherId:(nonnull NSString*) publisherId andAppId:(nonnull NSString*) appId;
@end
