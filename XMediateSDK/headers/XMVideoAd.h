//
//  XMVideoAd.h
//  XMediateSDK
//
//  Created by XMediate on 05/04/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMVideoAdDelegate.h"
#import "XMAdSettings.h"

@interface XMVideoAd : NSObject


-(instancetype) initWithDelegate:(id<XMVideoAdDelegate>)delegate;

/**
 * The delegate (`XMVideoAdDelegate`) of the video ad object.
 */
@property (nonatomic, weak) id<XMVideoAdDelegate> delegate;

/**
 * Indicates if the video ad is ready to be presented full screen.
 */
@property(nonatomic, readonly, getter=isReady) BOOL ready;

/**
 * Requests a new ad.
 *
 * If the video ad is already loading an ad or one is already presnted, this call will be ignored.
 */
-(void) loadWithSettings:(XMAdSettings*)adSettings;

/**
 * Presents the video ad modally from the specified view controller.
 *
 * This method will do nothing if the video ad has not been ready (i.e. the value of its `ready` property is NO).
 *
 * `XMVideoAdDelegate` provides optional methods that you may implement to stay
 * informed about when an  video takes over or leaves the screen.
 *
 * @param controller The view controller that should be used to present the video ad.
 * This view controller must be added to the application key window before you call this method.
 */
- (void)presentFromViewController:(UIViewController *)controller;

@end
