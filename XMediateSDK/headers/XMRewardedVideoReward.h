//
//  XMRewardedVideoReward.h
//  XMediateSDK
//
//  Created by XMediate on 23/03/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMRewardedVideoReward : NSObject

@property(strong, nonatomic) NSString *rewardType;
@property(strong, nonatomic) NSNumber *rewardAmount;

@end
