//
//  XMNativeAdEventsDelegate.h
//  XMediateSDK
//
//  Created by XMediate on 03/07/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>
@class XMNativeAd;
@protocol XMNativeAdEventsDelegate <NSObject>

@optional

-(void) recievedClickForNativeAd:(XMNativeAd *)ad;

-(void) willPresentScreenForNativeAd:(XMNativeAd *)ad;

-(void) didDismissScreenForNativeAd:(XMNativeAd *)ad;

-(void) willLeaveApplicationForNativeAd:(XMNativeAd *)ad;

@end
