//
//  XMNativeAdImage.h
//  XMediateSDK
//
//  Created by XMediate on 05/07/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMNativeAdImage : NSObject

@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) NSString *imageURL;

@end
