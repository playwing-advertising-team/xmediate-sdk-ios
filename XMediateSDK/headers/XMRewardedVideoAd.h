//
//  XMRewardedVideoAd.h
//  XMediateSDK
//
//  Created by XMediate on 23/03/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMAdSettings.h"
#import "XMRewardedVideoAdDelegate.h"

@interface XMRewardedVideoAd : NSObject

-(instancetype) initWithDelegate:(id<XMRewardedVideoAdDelegate>)delegate;

/**
 * The delegate (`XMRewardedVideoAdDelegate`) of the interstitial ad object.
 */
@property (nonatomic, weak) id<XMRewardedVideoAdDelegate> delegate;

/**
 * Indicates if the video ad is ready to be presented full screen.
 */
@property(nonatomic, readonly, getter=isReady) BOOL ready;

/**
 * Requests a new ad.
 *
 * If the rewarded video ad is already loading an ad or one is already presnted, this call will be ignored.
 */
-(void) loadWithSettings:(XMAdSettings*)adSettings;

/**
 * Presents the rewarded video ad modally from the specified view controller.
 *
 * This method will do nothing if the rewarded video ad has not been ready (i.e. the value of its `ready` property is NO).
 *
 * `XMRewardedVideoAdDelegate` provides optional methods that you may implement to stay
 * informed about when an rewarded video takes over or leaves the screen.
 *
 * @param controller The view controller that should be used to present the video ad.
 * This view controller must be added to the application key window before you call this method.
 */
- (void)presentFromViewController:(UIViewController *)controller;

@end
