//
//  XMNativeAdLoader.h
//  XMediateSDK
//
//  Created by Tarun Sharma on 09/06/17.
//  Copyright © 2017 XMediate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMNativeAdLoaderDelegate.h"
#import "XMAdSettings.h"

@interface XMNativeAdLoader : NSObject

@property(nonatomic, weak) id<XMNativeAdLoaderDelegate> delegate;

-(instancetype) initWithDelegate:(id<XMNativeAdLoaderDelegate>)delegate;

/**
 * Requests a new native ad.
 *
 * If a native is already loading this call will be ignored.
 */
-(void) loadWithSettings:(XMAdSettings*)adSettings;

@end
